#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Original work on certificate parsing from the slixmpp project at
# https://git.louiz.org/slixmpp licensed under the MIT license.
# Copyright © 2010 Nathanael C. Fritz
#
# Copyright © 2017 Collabora Ltd.
# Copyright © 2017 Maxime “pep” Buquet <pep@collabora.com>
#
# Distributed under terms of the LGPL-2.1+. See the LICENSE file.

"""
    Certificate class
"""

import re
import ssl
from datetime import datetime
from pyasn1.codec.der import decoder
from pyasn1.type.univ import OctetString
from pyasn1.type.char import BMPString
from pyasn1.type.useful import GeneralizedTime
import pyasn1.error
from pyasn1_modules.rfc2459 import Certificate as CertSpec, DirectoryString
from pyasn1_modules.rfc2459 import SubjectAltName
from pyasn1_modules.rfc2459 import id_ce_subjectAltName as SUBJECT_ALT_NAME
from pyasn1_modules.rfc2459 import id_at_commonName as COMMON_NAME

import logging

log = logging.getLogger(__name__)

PEM_RE = re.compile(
    '-----BEGIN CERTIFICATE-----.*?-----END CERTIFICATE-----',
    flags=re.DOTALL
)


class InvalidCertificateFormat(Exception):
    def __init__(self, filename):
        self.filename = filename


def read_file(filename):
    content = None

    try:
        # Look for PEM, text file
        log.debug('Trying PEM format')
        with open(filename, 'r') as f:
            pem = f.read()

        content = ''.join(PEM_RE.findall(pem))
        content = ssl.PEM_cert_to_DER_cert(content)
    except (ValueError, UnicodeDecodeError):
        log.debug('Trying DER format')
        with open(filename, 'rb') as f:
            der = f.read()
        content = der

    # Check we're dealing with the correct format
    try:
        decoder.decode(content, asn1Spec=CertSpec())[0]
    except (pyasn1.error.PyAsn1Error, pyasn1.error.SubstrateUnderrunError):
        raise InvalidCertificateFormat(filename)

    return content


def decode_str(data):
    encoding = 'utf-16-be' if isinstance(data, BMPString) else 'utf-8'
    return bytes(data).decode(encoding)


def timefield_to_date(data):
    format = '%y%m%d%H%M%SZ'
    if isinstance(data, GeneralizedTime):
        format = '%Y%m%d%H%M%SZ'

    return datetime.strptime(data, format)


class Certificate:
    def __init__(self, filename):
        raw_cert = read_file(filename)
        cert = decoder.decode(raw_cert, asn1Spec=CertSpec())[0]
        self._tbsCertificate = cert.getComponentByName('tbsCertificate')

    def _extensions(self):
        return self._tbsCertificate.getComponentByName('extensions') or []

    def notBefore(self):
        validity = self._tbsCertificate.getComponentByName('validity')
        notBefore = validity.getComponentByName('notBefore').getComponent()
        return timefield_to_date(str(notBefore))

    def notAfter(self):
        validity = self._tbsCertificate.getComponentByName('validity')
        notAfter = validity.getComponentByName('notAfter').getComponent()
        return timefield_to_date(str(notAfter))

    def _commonName(self, seq):
        for rdnss in seq:
            for rdns in rdnss:
                for name in rdns:
                    oid = name.getComponentByName('type')
                    val = name.getComponentByName('value')

                    if oid != COMMON_NAME:
                        continue

                    val = decoder.decode(val, asn1Spec=DirectoryString())[0]
                    val = decode_str(val.getComponent())
                    return val

    def issuerCommonName(self):
        issuer = self._tbsCertificate.getComponentByName('issuer')
        return self._commonName(issuer)

    def subjectCommonName(self):
        subject = self._tbsCertificate.getComponentByName('subject')
        return self._commonName(subject)

    def subjectAltName(self, **kwargs):
        filters = kwargs.get('filters')
        if filters is None:
            filters = ['dNSName']

        SAN = set()

        for extension in self._extensions():
            oid = extension.getComponentByName('extnID')
            if oid != SUBJECT_ALT_NAME:
                continue

            value = decoder.decode(extension.getComponentByName('extnValue'),
                                   asn1Spec=OctetString())[0]
            sa_names = decoder.decode(value, asn1Spec=SubjectAltName())[0]
            for name in sa_names:
                name_type = name.getName()
                if name_type in filters:
                    SAN.add(decode_str(name.getComponent()))

        return list(SAN)
