#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Collabora Ltd.
# Copyright © 2017 Maxime “pep” Buquet <pep@collabora.com>
#
# Distributed under terms of the LGPL-2.1+. See the LICENSE file.

"""
"""

from setuptools import setup

setup(
    name='certcheck',
    version='0.2.2',
    license='LGPL-2.1+',
    url='https://git.collabora.com/cgit/users/ppjet/certcheck.git',
    packages=[
        'certcheck',
    ],
    entry_points={
        'console_scripts': [
            'certexpiry = certcheck:expiry_command',
            'certattr = certcheck:attr_command',
            'certdomains = certcheck:diff_command',
        ],
    },
    author='Maxime “pep” Buquet',
    author_email='maxime.buquet@collabora.co.uk',
    classifiers=[
        'License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
)
