#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Collabora Ltd.
# Copyright © 2017 Maxime “pep” Buquet <pep@collabora.com>
#
# Distributed under terms of the LGPL-2.1+. See the LICENSE file.

"""
    x509 Certificate Utility Checks
"""

from .certificate import Certificate, InvalidCertificateFormat

import sys
import logging
import argparse
from datetime import datetime

log = logging.getLogger(__name__)


def _base_parser():
    parser = argparse.ArgumentParser(
        description='x509 Certificate Utility checks')

    parser.add_argument(
        '--quiet', '-q', help='set logging to error',
        action='store_const', dest='loglevel',
        const=logging.ERROR, default=logging.INFO
    )
    parser.add_argument(
        '--debug', '-d', help='set logging to debug',
        action='store_const', dest='loglevel',
        const=logging.DEBUG, default=logging.INFO
    )

    return parser


def compare_domains(cert, domains):
    cert_domains = set(cert.subjectAltName(filters=['dNSName']))
    cert_domains.add(cert.subjectCommonName())
    domains = set(domains)
    log.debug('Domains in certificate: %r', cert_domains)
    log.debug('Domains to diff against: %r', domains)
    return cert_domains == domains


def diff_command():
    log.debug('Diff command')
    parser = _base_parser()

    parser.add_argument(
        '--file', '-f', action='store', type=str,
        help='File to compare', dest='filename', required=True,
    )
    parser.add_argument(
        '--dd', '--diff', '--domains', action='store', nargs='+', type=str,
        help='Domains to diff against', dest='domains', required=True,
    )

    args = parser.parse_args(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)

    log.debug('File: %s', args.filename)

    try:
        cert = Certificate(args.filename)
    except (FileNotFoundError,) as e:
        print('File not found: %s' % e.filename, file=sys.stderr)
        sys.exit(1)
    except (InvalidCertificateFormat,) as e:
        print('Incorrect file format: %s' % e.filename, file=sys.stderr)
        sys.exit(1)

    result = compare_domains(cert, args.domains)
    log.debug('Matching: %r', result)

    sys.exit(0 if result else 1)


def attr_command():
    log.debug('Attr command')
    parser = _base_parser()

    parser.add_argument(
        '--attr', '-a', action='store', type=str,
        choices=['issuerCN', 'subjectCN', 'notAfter'],
        help="'issuerCN', 'subjectCN', or 'notAfter'", required=True,
    )
    parser.add_argument(
        '--files', '-f',  action='store', nargs='+', type=str,
        help='Certificates to use', default=[], dest='files', required=True,
    )

    args = parser.parse_args(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)

    attrs = {
        'issuerCN': lambda c: c.issuerCommonName(),
        'subjectCN': lambda c: c.subjectCommonName(),
        'notAfter': lambda c: str(c.notAfter()),
    }

    for filename in args.files:
        try:
            cert = Certificate(filename)
        except (FileNotFoundError,) as e:
            print('File not found: %s' % e.filename, file=sys.stderr)
            sys.exit(1)
        except (InvalidCertificateFormat,) as e:
            print('Incorrect file format: %s' % e.filename, file=sys.stderr)
            sys.exit(1)

        print('%s: %s' % (filename, attrs[args.attr](cert)))

    sys.exit(0)


def expiry_command():
    log.debug('Expiry command')
    parser = _base_parser()

    parser.add_argument(
        '--files', '-f',  action='store', nargs='+', type=str,
        help='Certificates to use', default=[], dest='files', required=True,
    )

    parser.add_argument(
        '--status', '-s',  action='store_true',
        help='Display VALID, EXPIRING or INVALID alongside the number of days',
    )

    def positive_int(s):
        val = int(s)
        if val <= 0:
            msg = "%r is not a positive integer" % s
            raise argparse.ArgumentTypeError(msg)
        return val

    parser.add_argument(
        '--days', action='store', type=positive_int, default=30,
        help=('Set the number of days for the "EXPIRING" status of --status, '
              'must be > 0'),
    )

    parser.add_argument(
        '--lowest', action='store_true',
        help='Only display entry with the lowest number of days left',
    )

    parser.add_argument(
        '--days-only', action='store_true',
        help=('Only display an integer for the number of days left. '
              'This overrides --status.'),
    )

    args = parser.parse_args(sys.argv[1:])
    logging.basicConfig(level=args.loglevel)

    if args.status and args.days <= 0:
        print('The number of days (%d) must be > 0' % args.days)
        sys.exit(1)

    FORMAT = '{filename}: {days} days'
    if args.days_only:
        FORMAT = '{days}'
    elif args.status:
        FORMAT = '{filename}: {status}; {days} days'

    lowest = {'filename': '', 'status': '', 'days': -1}

    for filename in args.files:
        try:
            cert = Certificate(filename)
        except (FileNotFoundError,) as e:
            print('File not found: %s' % e.filename, file=sys.stderr)
            sys.exit(1)
        except (InvalidCertificateFormat,) as e:
            print('Incorrect file format: %s' % e.filename, file=sys.stderr)
            sys.exit(1)

        notAfter = cert.notAfter()
        log.debug('%s: %s', filename, notAfter)
        delta = notAfter - datetime.now()

        status = 'INVALID'
        if delta.days > args.days:
            status = 'VALID'
        elif 0 < delta.days <= args.days:
            status = 'EXPIRING'

        values = {
            'filename': filename,
            'status': status,
            'days': delta.days,
        }

        if lowest['filename'] == '' or delta.days < lowest['days']:
            lowest = values

        if not args.lowest:
            print(FORMAT.format(**values))

    if args.lowest:
        print(FORMAT.format(**lowest))

    sys.exit(0)
